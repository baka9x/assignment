package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nhbaka.assignment.Model.Student;
import com.nhbaka.assignment.SQLite.StudentDAO;
import com.nhbaka.assignment.SQLite.UserDAO;

public class StartActivity extends AppCompatActivity {
    Button btnRegister, btnLogin;
    Intent intent;
    UserDAO userDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);


        btnRegister = findViewById(R.id.btnRegister);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister.setVisibility(View.GONE);
        userDAO = new UserDAO(this);
        if (!userDAO.isNull()) {
            userDAO.insertAdmin();
        } else {
            //Toast.makeText(this, "Tài khoản Admin là: baka mật khẩu asdasd", Toast.LENGTH_SHORT).show();
            Log.d("Check", "Đã có tài khoản admin là baka mật khẩu asdasd = " + userDAO.isNull());
            //Đã đăng nhập
            if (!userDAO.isOnline().equals("")) {
                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(this, "Bạn đã đăng nhập", Toast.LENGTH_SHORT).show();

            }
        }




        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(StartActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(StartActivity.this, RegisterActivity.class);
                startActivity(intent);

            }
        });


    }
}
