package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nhbaka.assignment.Model.Student;
import com.nhbaka.assignment.Model.User;
import com.nhbaka.assignment.SQLite.StudentDAO;
import com.nhbaka.assignment.SQLite.UserDAO;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    EditText username, password, rePassword;
    Button btnRegister;
    UserDAO userDAO;
    ArrayList<Student> list;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Thông tin tài khoản");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        rePassword = findViewById(R.id.rePassword);
        btnRegister = findViewById(R.id.btnRegister);
        userDAO = new UserDAO(this);
        list = new ArrayList<>();


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                String txtUsername = username.getText().toString();
                String txtPassword = password.getText().toString();
                String txtRePassword = rePassword.getText().toString();
                //username.setError("Lỗi");

                //Check validatetion
                if (TextUtils.isEmpty(txtUsername) || TextUtils.isEmpty(txtPassword) || TextUtils.isEmpty(txtRePassword)) {
                    Toast.makeText(RegisterActivity.this, "Bạn chưa nhập thông tin đăng ký", Toast.LENGTH_SHORT).show();
                } else if (txtPassword.length() < 6 || txtPassword.length() > 18) {
                    Toast.makeText(RegisterActivity.this, "Mật khẩu độ dài từ 6 đến 18 ký tự", Toast.LENGTH_SHORT).show();
                } else if (!txtPassword.equals(txtRePassword)) {
                    Toast.makeText(RegisterActivity.this, "Mật khẩu nhập lại không đúng", Toast.LENGTH_SHORT).show();
                } else {

                    user.setStdCode(txtUsername.trim());
                    user.setStdPassword(txtPassword.trim());
                    if (user.getStdCode().equals("baka")) {
                        user.setType(9);
                    } else {
                        user.setType(1);
                    }
                    if (userDAO.insertUser(user) == -1) {
                        Toast.makeText(RegisterActivity.this, "Đăng ký không thành công", Toast.LENGTH_SHORT).show();

                    }
                    //Thành công
                    //list = userDAO.addUser();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();


                }


            }
        });
    }


}
