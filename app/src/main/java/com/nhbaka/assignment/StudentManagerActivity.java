package com.nhbaka.assignment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nhbaka.assignment.Adapter.ClassroomAdapter;
import com.nhbaka.assignment.Adapter.StudentAdapter;
import com.nhbaka.assignment.Model.Classroom;
import com.nhbaka.assignment.Model.Student;
import com.nhbaka.assignment.Model.User;
import com.nhbaka.assignment.SQLite.ClassroomDAO;
import com.nhbaka.assignment.SQLite.StudentDAO;
import com.nhbaka.assignment.SQLite.UserDAO;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;

public class StudentManagerActivity extends AppCompatActivity {

    EditText student, studentCode;
    TextView birthday, tvStdCode, tvMasv;
    Spinner spinnerCl;
    Button btnAddStudent, btnEditStudent, btnRemoveStudent, btnExport;
    ListView lvStudent;
    StudentDAO studentDAO;
    ClassroomDAO classroomDAO;
    UserDAO userDAO;
    StudentAdapter studentAdapter;
    ClassroomAdapter roomAdapter;
    ArrayList<Student> list;
    ArrayList<Classroom> room;


    private static final int REQUEST_CODE = 999;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    final String sdCard = Environment
            .getExternalStorageDirectory()
            .getAbsolutePath() + "/Ds-lop.csv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_manager);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Quản lý sinh viên");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        student = findViewById(R.id.student);
        birthday = findViewById(R.id.birthday);
        spinnerCl = findViewById(R.id.spinnerCl);
        btnAddStudent = findViewById(R.id.btnAddStudent);
        lvStudent = findViewById(R.id.lvStudent);
        studentCode = findViewById(R.id.studentCode);
        btnEditStudent = findViewById(R.id.btnEditStudent);
        btnRemoveStudent = findViewById(R.id.btnRemoveStudent);
        tvStdCode = findViewById(R.id.tvStdCode);
        tvMasv = findViewById(R.id.tvMasv);
        btnExport = findViewById(R.id.btnExport);


        //Khai báo data
        list = new ArrayList<>();
        studentDAO = new StudentDAO(this);
        userDAO = new UserDAO(this);


        //is User normal
        if (!userDAO.isAdmin(userDAO.isOnline())) {
            student.setVisibility(View.GONE);
            birthday.setVisibility(View.GONE);
            btnAddStudent.setVisibility(View.GONE);
            btnEditStudent.setVisibility(View.GONE);
            btnRemoveStudent.setVisibility(View.GONE);
            studentCode.setVisibility(View.GONE);
        }

        //Birthday
        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        StudentManagerActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                //Log.d(TAG, "onDataSet: date: " + i + "-" + i1 + "-" + i2);
                int year = i;
                int months = i1 + 1;// + 1 cho tháng
                int days = i2;
                String day = "" + days;
                String month = "" + months;
                if (months <= 9) {
                    month = "0" + months;
                }
                if (days <= 9) {
                    day = "0" + days;
                }
                String date = month + "-" + day + "-" + year;//Định dạng mm/dd/yyyy
                birthday.setText(date);
            }
        };


        //Get data to Spinner
        classroomDAO = new ClassroomDAO(this);
        room = new ArrayList<>();
        room = classroomDAO.addClassRoom();//Giá trị room trong ArrayList
        roomAdapter = new ClassroomAdapter(this, room);
        spinnerCl.setAdapter(roomAdapter);

        final Student std = new Student();
        final User users = new User();

        spinnerCl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {//i = position

                //Get mã lớp
                final String txtClassCode = room.get(i).getClassCode();
                Log.d("Mã lớp", txtClassCode);
                std.setClassCode(txtClassCode);


                //GONE
                tvStdCode.setVisibility(View.GONE);
                tvMasv.setVisibility(View.GONE);
                studentCode.setVisibility(View.VISIBLE);
                studentCode.requestFocus();

                //Xóa trắng
                student.setText("");
                birthday.setText("Ngày sinh (Nhấn vào để chọn)");

                //Tự động hiển thị danh sách Student theo lớp
                list = studentDAO.checkStudent(txtClassCode);//DAOstudent để hiện sinh viên theo mã lớp
                studentAdapter = new StudentAdapter(StudentManagerActivity.this, list);
                lvStudent.setAdapter(studentAdapter);


                //Xuất file Csv Excel
                btnExport.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityCompat.requestPermissions(
                                StudentManagerActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_CODE
                        );

                    }
                });


                //Thêm sinh viên
                btnAddStudent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String txtName = student.getText().toString();
                        String txtBirthday = birthday.getText().toString();
                        String txtCode = studentCode.getText().toString().trim().toLowerCase();//Masv chuyển về chữ thường

                        //Check validate
                        if (TextUtils.isEmpty(txtCode) || TextUtils.isEmpty(txtName) || txtBirthday.equals("Ngày sinh (Nhấn vào để chọn)")) {
                            Toast.makeText(StudentManagerActivity.this, "Vui lòng không bỏ trống.", Toast.LENGTH_SHORT).show();
                        }
                        //Check trùng
                        else if (studentDAO.isDuplicateStd(txtCode, txtClassCode, txtName, txtBirthday)) {
                            Toast.makeText(StudentManagerActivity.this, "Bị trùng sinh viên", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("ghghg", studentDAO.isDuplicateStd(txtCode, txtClassCode, txtName, txtBirthday) + "");
                            //Xóa trắng
                            studentCode.setText("");
                            student.setText("");
                            birthday.setText("Ngày sinh (Nhấn vào để chọn)");
                            int count = userDAO.isUsed(txtCode) + 1;
                            //Thêm sinh viên
                            std.setStdCode(txtCode);
                            std.setStdName(txtName);
                            std.setBirthday(txtBirthday);
                            std.setIsUsed(count);
                            //Thêm user
                            String email = txtCode + "@nhbaka.com";
                            users.setStdCode(txtCode);
                            users.setEmail(email);
                            users.setIsUsed(count);
                            Log.d("Get số lớp đã học ", "" + count);
                            userDAO.updateIsUsed(users, count);
                            studentDAO.updateIsUsed(std, count);
                            if (studentDAO.insertStudent(std) == -1) {
                                Toast.makeText(StudentManagerActivity.this, "Thêm không thành công", Toast.LENGTH_SHORT).show();
                            }
                            if (userDAO.insertUser(users) == -1) {
                                Toast.makeText(StudentManagerActivity.this, "Tài khoản đã được tạo trước đó", Toast.LENGTH_SHORT).show();
                            }
                            //Thành công
                            //Tự động hiển thị danh sách Student theo lớp
                            list.add(std);
                            list = studentDAO.checkStudent(txtClassCode);//Gọi lại DAO
                            studentAdapter = new StudentAdapter(StudentManagerActivity.this, list);
                            lvStudent.setAdapter(studentAdapter);
                            //studentDAO.writeCsv(sdCard, list);

                        }

                    }
                });

                //Sửa sinh viên
                btnEditStudent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String txtName = student.getText().toString();
                        String txtBirthday = birthday.getText().toString();
                        String txtCode = tvStdCode.getText().toString().trim();

                        //Check validate
                        if (TextUtils.isEmpty(txtCode) || TextUtils.isEmpty(txtName) || txtBirthday.equals("Ngày sinh (Nhấn vào để chọn)")) {
                            Toast.makeText(StudentManagerActivity.this, "Vui lòng không bỏ trống.", Toast.LENGTH_SHORT).show();
                        } else {
                            tvStdCode.setVisibility(View.GONE);
                            tvMasv.setVisibility(View.GONE);
                            studentCode.setVisibility(View.VISIBLE);

                            //Xóa trắng
                            student.setText("");
                            birthday.setText("Ngày sinh (Nhấn vào để chọn)");

                            //Thêm sinh viên
                            std.setStdCode(txtCode);
                            std.setStdName(txtName);
                            std.setBirthday(txtBirthday);
                            if (studentDAO.updateStudent(std) == -1) {
                                Toast.makeText(StudentManagerActivity.this, "Sửa không thành công", Toast.LENGTH_SHORT).show();
                            }
                            //Thành công
                            list.add(std);
                            list = studentDAO.checkStudent(txtClassCode);
                            studentAdapter = new StudentAdapter(StudentManagerActivity.this, list);
                            lvStudent.setAdapter(studentAdapter);


                        }

                    }
                });

                //Xóa sinh viên
                btnRemoveStudent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String txtName = student.getText().toString();
                        String txtCode = tvStdCode.getText().toString().trim();

                        //Check validate
                        if (TextUtils.isEmpty(txtCode) || TextUtils.isEmpty(txtName)) {
                            Toast.makeText(StudentManagerActivity.this, "Vui lòng không bỏ trống.", Toast.LENGTH_SHORT).show();
                        } else {
                            tvStdCode.setVisibility(View.GONE);
                            tvMasv.setVisibility(View.GONE);
                            studentCode.setVisibility(View.VISIBLE);

                            //Xóa trắng
                            student.setText("");
                            birthday.setText("Ngày sinh (Nhấn vào để chọn)");
                            //Thêm sinh viên
                            std.setStdCode(txtCode);
                            std.setClassCode(txtClassCode);
                            //Xóa user
                            int count = userDAO.isUsed(txtCode);

                            if (count >= 1) {
                                count = count - 1;
                                std.setIsUsed(count);
                                users.setIsUsed(count);
                                Log.d("Get số lớp đã học ", "" + count);
                                userDAO.updateIsUsed(users, count);
                                studentDAO.updateIsUsed(std, count);
                            }
                            if (studentDAO.removeStudent(std) == -1) {
                                Toast.makeText(StudentManagerActivity.this, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                            }
                            if (userDAO.isUsedd(txtCode) <= 1) {
                                if (userDAO.removeUser(users)) {
                                    Toast.makeText(StudentManagerActivity.this, "Xóa tài khoản kèm theo thành công", Toast.LENGTH_SHORT).show();
                                    Log.d("Lỗi", "" + userDAO.removeUser(users));
                                }
                            } else {
                                Toast.makeText(StudentManagerActivity.this, "Tài khoản sinh viên còn đang sử dụng", Toast.LENGTH_SHORT).show();
                            }


                            //Thành công
                            list.add(std);
                            list = studentDAO.checkStudent(txtClassCode);
                            studentAdapter = new StudentAdapter(StudentManagerActivity.this, list);
                            lvStudent.setAdapter(studentAdapter);


                        }

                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //Tự động hiển thị danh sách Student theo lớp
                list = studentDAO.addStudent();
                studentAdapter = new StudentAdapter(StudentManagerActivity.this, list);
                lvStudent.setAdapter(studentAdapter);

            }
        });
        //Click lv lên chỉnh sửa
        lvStudent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Student sv = list.get(i);
                tvMasv.setVisibility(View.VISIBLE);
                tvStdCode.setVisibility(View.VISIBLE);
                studentCode.setVisibility(View.GONE);
                tvStdCode.setText(sv.getStdCode());
                student.setText(sv.getStdName());
                birthday.setText(sv.getBirthday());


            }
        });


    }

    //Chọn gallery
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Get vị trí của spinner
                int malop = spinnerCl.getSelectedItemPosition();
                String txtMalop = room.get(malop).getClassCode();
                list = studentDAO.checkStudent(txtMalop);
                studentDAO.writeCsv(sdCard, list);
                Toast.makeText(this, "Ghi file thành công", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Bạn không có quyền truy cập file!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onCheck(){
        int i = 0;
        i++;
        //Increased i times

        return false;
    }



}
