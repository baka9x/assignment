package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nhbaka.assignment.Model.Student;
import com.nhbaka.assignment.Model.User;
import com.nhbaka.assignment.SQLite.StudentDAO;
import com.nhbaka.assignment.SQLite.UserDAO;

public class ChangePasswordActivity extends AppCompatActivity {

    TextInputEditText newPassword, rePassword;
    Button btnChangePassword;
    UserDAO userDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change);

        newPassword = findViewById(R.id.newPassword);
        rePassword = findViewById(R.id.rePassword);
        btnChangePassword = findViewById(R.id.btnChangePassword);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Đổi mật khẩu");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        userDAO = new UserDAO(this);
        final User user = new User();


        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txtNewPassWord = newPassword.getText().toString();
                Log.d("", txtNewPassWord);
                String txtRePassword = rePassword.getText().toString();

                //Validate
                if (TextUtils.isEmpty(txtNewPassWord) || TextUtils.isEmpty(txtRePassword)) {
                    Toast.makeText(ChangePasswordActivity.this, "Vui lòng nhập đủ thông tin", Toast.LENGTH_SHORT).show();
                } else if (!txtRePassword.equals(txtNewPassWord)) {
                    Toast.makeText(ChangePasswordActivity.this, "Mật khẩu nhập lại không đúng", Toast.LENGTH_SHORT).show();
                } else {
                    user.setStdCode(userDAO.isOnline().trim());
                    user.setStdPassword(txtNewPassWord);
                    userDAO.updatePassword(user);
                    Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                    Toast.makeText(ChangePasswordActivity.this, "Đổi mật khẩu thành công, Vui lòng đăng nhập lại", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                    finish();
                }

            }
        });


    }
}
