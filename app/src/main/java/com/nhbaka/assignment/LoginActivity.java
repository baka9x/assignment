package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nhbaka.assignment.Model.Student;
import com.nhbaka.assignment.Model.User;
import com.nhbaka.assignment.SQLite.StudentDAO;
import com.nhbaka.assignment.SQLite.UserDAO;

public class LoginActivity extends AppCompatActivity {

    EditText username;
    TextInputEditText password;
    Button btnLogin, btnClear;
    UserDAO userDAO;
    CheckBox checkSave;
    SharedPreferences data;
    TextView forgetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Thông tin tài khoản");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btnLogin);
        btnClear = findViewById(R.id.btnClear);
        checkSave = findViewById(R.id.checkSave);
        forgetPassword = findViewById(R.id.forgetPassword);


        data = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        //Nạp thông tin lên form từ sharedPreference
        Boolean dataSave = data.getBoolean("save_information", false);
        if (dataSave) {
            username.setText(data.getString("username", ""));
            password.setText(data.getString("password", ""));
            checkSave.setChecked(true);
        }


        //Xử lý click
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username.setText("");
                password.setText("");
                checkSave.setChecked(false);
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                intent.putExtra("USERNAME", username.getText().toString().trim());
                startActivity(intent);
            }
        });


        userDAO = new UserDAO(this);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txtUsername = username.getText().toString();
                String txtPassword = password.getText().toString();


                if (TextUtils.isEmpty(txtUsername) || TextUtils.isEmpty(txtPassword)) {
                    Toast.makeText(LoginActivity.this, "Fields không được bỏ trống", Toast.LENGTH_SHORT).show();
                } else {

                    String pass = userDAO.checkPass(txtUsername);
                    if (txtPassword.equals(pass)) {
                        User user = new User();
                        user.setStdCode(txtUsername);
                        userDAO.updateOnline(user, 1);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        Intent intent1 = new Intent(LoginActivity.this, ChangePasswordActivity.class);
                        //Check username
                        if (txtPassword.equals("123456")) {
                            startActivity(intent1);
                        } else {
                            startActivity(intent);
                        }
                        Toast.makeText(LoginActivity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();

                        SharedPreferences.Editor editor = data.edit();

                        if (checkSave.isChecked()) {
                            //Luu tru thong tin vao file

                            editor.putString("username", txtUsername);
                            editor.putString("password", txtPassword);

                        }
                        editor.putBoolean("save_information", true);
                        editor.apply();


                    } else {

                        Toast.makeText(LoginActivity.this, "Sai tài khoản hoặc mật khẩu", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }


}
