package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhbaka.assignment.Adapter.ClassroomAdapter;
import com.nhbaka.assignment.Model.Classroom;
import com.nhbaka.assignment.SQLite.ClassroomDAO;
import com.nhbaka.assignment.SQLite.StudentDAO;
import com.nhbaka.assignment.SQLite.UserDAO;

import java.util.ArrayList;

public class ShowClassActivity extends AppCompatActivity {

    ListView lvClassRoom;
    ClassroomAdapter clAdapter;
    ArrayList<Classroom> list;
    ClassroomDAO classroomDAO;
    UserDAO userDAO;
    StudentDAO studentDAO;
    TextView tvMalop, tvClassCode;
    EditText edtClassName;
    Button btnEdit, btnRemove;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_class);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Danh sách lớp");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        lvClassRoom = findViewById(R.id.lvClassRoom);
        tvMalop = findViewById(R.id.tvMalop);
        tvClassCode = findViewById(R.id.tvClassCode);
        edtClassName = findViewById(R.id.edtClassName);
        btnEdit = findViewById(R.id.btnEdit);
        btnRemove = findViewById(R.id.btnRemove);


        //Khai báo data
        userDAO = new UserDAO(this);
        classroomDAO = new ClassroomDAO(this);
        studentDAO = new StudentDAO(this);
        list = new ArrayList<>();
        list = classroomDAO.addClassRoom();
        clAdapter = new ClassroomAdapter(this, list);
        lvClassRoom.setAdapter(clAdapter);


        //is Normal user
        if (!userDAO.isAdmin(userDAO.isOnline())) {
            edtClassName.setVisibility(View.GONE);
            btnEdit.setVisibility(View.GONE);
            btnRemove.setVisibility(View.GONE);
        }



        lvClassRoom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Classroom room = list.get(i);
                tvMalop.setVisibility(View.VISIBLE);
                tvClassCode.setVisibility(View.VISIBLE);

                tvClassCode.setText(room.getClassCode());
                edtClassName.setText(room.getClassName());

            }
        });

        edtClassName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchRooms(charSequence.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Classroom classroom = new Classroom();
                String txtClassCode = tvClassCode.getText().toString().trim();
                String txtClassName = edtClassName.getText().toString().trim();

                //Check validate
                if (TextUtils.isEmpty(txtClassCode)|| TextUtils.isEmpty(txtClassName)) {
                    Toast.makeText(ShowClassActivity.this, "Vui lòng không bỏ trống.", Toast.LENGTH_SHORT).show();
                } else {

                    //Xóa trắng
                    tvClassCode.setText("Chưa có mã lớp");
                    edtClassName.setText("");
                    tvMalop.setVisibility(View.GONE);
                    tvClassCode.setVisibility(View.GONE);
                    //Thêm lớp vào list
                    classroom.setClassCode(txtClassCode);
                    classroom.setClassName(txtClassName);
                    if (classroomDAO.updateRoom(classroom) == -1) {
                        Toast.makeText(ShowClassActivity.this, "Sửa không thành công", Toast.LENGTH_SHORT).show();
                    }
                    //Thành công
                    list.add(classroom);
                    list = classroomDAO.addClassRoom();
                    clAdapter = new ClassroomAdapter(ShowClassActivity.this, list);
                    lvClassRoom.setAdapter(clAdapter);


                }


            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Classroom classroom = new Classroom();
                String txtClassCode = tvClassCode.getText().toString().trim();
                String txtClassName = edtClassName.getText().toString().trim();

                //Check validate
                if (TextUtils.isEmpty(txtClassCode)|| TextUtils.isEmpty(txtClassName)) {
                    Toast.makeText(ShowClassActivity.this, "Vui lòng không bỏ trống.", Toast.LENGTH_SHORT).show();
                } else {

                    //Xóa trắng
                    tvClassCode.setText("Chưa có mã lớp");
                    edtClassName.setText("");
                    tvMalop.setVisibility(View.GONE);
                    tvClassCode.setVisibility(View.GONE);


                    //Thêm lớp vào list
                    classroom.setClassCode(txtClassCode);

                    if (studentDAO.isUsed(txtClassCode)){
                        if (classroomDAO.removeRoom(classroom) == -1) {
                            Toast.makeText(ShowClassActivity.this, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(ShowClassActivity.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(ShowClassActivity.this, "Vui lòng xóa các sinh viên trong lớp này trước khi xóa", Toast.LENGTH_SHORT).show();
                    }


                    //Thành công
                    list.add(classroom);
                    list = classroomDAO.addClassRoom();
                    clAdapter = new ClassroomAdapter(ShowClassActivity.this, list);
                    lvClassRoom.setAdapter(clAdapter);


                }


            }
        });




    }
    //Tìm kiếm lớp (CHƯA LÀM)
    private void searchRooms(String toString) {
        Classroom room = new Classroom();

        if (toString.equals(room.getClassName())){
            classroomDAO.searchRooms(toString);
            list = new ArrayList<>();
            list.add(room);
            list = classroomDAO.addClassRoom();
            clAdapter = new ClassroomAdapter(ShowClassActivity.this, list);
            lvClassRoom.setAdapter(clAdapter);
        }

    }
}
