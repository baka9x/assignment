package com.nhbaka.assignment.Model;

public class Classroom {

    private String classCode;
    private String className;

    public Classroom(String classCode, String className) {

        this.classCode = classCode;
        this.className = className;
    }

    public Classroom() {
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
