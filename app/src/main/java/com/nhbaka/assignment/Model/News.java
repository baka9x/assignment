package com.nhbaka.assignment.Model;

public class News {
    private String title;
    private String date;
    private String link;
    private String thumbnail;

    public News() {
    }

    public News(String title, String date, String link, String thumbnail) {
        this.title = title;
        this.date = date;
        this.link = link;
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
