package com.nhbaka.assignment.Model;

public class User {
    private String stdCode;
    private String stdPassword;
    private String email;
    private int type;
    private int isOnline;
    private int isUsed;

    public User(String stdCode, String stdPassword, String email, int type, int isOnline, int isUsed) {
        this.stdCode = stdCode;
        this.stdPassword = stdPassword;
        this.email = email;
        this.type = type;
        this.isOnline = isOnline;
        this.isUsed = isUsed;
    }

    public User() {
    }

    public String getStdCode() {
        return stdCode;
    }

    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    public String getStdPassword() {
        return stdPassword;
    }

    public void setStdPassword(String stdPassword) {
        this.stdPassword = stdPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(int isOnline) {
        this.isOnline = isOnline;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }
}

