package com.nhbaka.assignment.Model;

public class Student {

    private String stdCode;
    private String stdName;
    private String birthday;
    private String classCode;
    private int isUsed;

    public Student(String stdCode, String stdName, String birthday, String classCode, int isUsed) {
        this.stdCode = stdCode;
        this.stdName = stdName;
        this.birthday = birthday;
        this.classCode = classCode;
        this.isUsed = isUsed;
    }

    public Student() {
    }

    public String getStdCode() {
        return stdCode;
    }

    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }
}
