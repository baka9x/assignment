package com.nhbaka.assignment.Model;

public class Schedule {
    private int id;
    private String name;
    private String courseName;
    private String date;
    private String duration;

    public Schedule() {
    }

    public Schedule(int id, String name, String courseName, String date, String duration) {
        this.id = id;
        this.name = name;
        this.courseName = courseName;
        this.date = date;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
