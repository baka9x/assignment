package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.nhbaka.assignment.Model.User;
import com.nhbaka.assignment.SQLite.UserDAO;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddClass;
    Button btnShowClass;
    Button btnManager;
    Button btnNews, btnMaps, btnSocial;
    UserDAO userDAO;
    Intent intent;
    TextView username, signout, changePassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAddClass = findViewById(R.id.btnAddClass);
        btnShowClass = findViewById(R.id.btnShowClass);
        btnManager = findViewById(R.id.btnManager);
        btnNews = findViewById(R.id.btnNews);
        btnMaps = findViewById(R.id.btnMaps);
        btnSocial = findViewById(R.id.btnSocial);
        username = findViewById(R.id.username);
        signout = findViewById(R.id.signout);
        changePassword = findViewById(R.id.changePassword);




        userDAO = new UserDAO(this);


        username.setText("Xin chào bạn, " + userDAO.isOnline().trim());



        btnAddClass.setOnClickListener(this);
        btnShowClass.setOnClickListener(this);
        btnManager.setOnClickListener(this);
        btnNews.setOnClickListener(this);
        btnMaps.setOnClickListener(this);
        btnSocial.setOnClickListener(this);



        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                user.setStdCode(userDAO.isOnline().trim());
                userDAO.updateOnline(user, 0);
                intent = new Intent(MainActivity.this, StartActivity.class);
                startActivity(intent);
                finish();
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String action;
                intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });


        if (!userDAO.isAdmin(userDAO.isOnline().trim())) {
            btnAddClass.setVisibility(View.GONE);


        } else {
            btnAddClass.setVisibility(View.VISIBLE);

        }




    }


    @Override
    public void onClick(View view) {
        int id = view.getId();


        switch (id) {
            case R.id.btnAddClass:
                intent = new Intent(MainActivity.this, AddClassActivity.class);
                startActivity(intent);
                break;
            case R.id.btnShowClass:
                intent = new Intent(MainActivity.this, ShowClassActivity.class);
                startActivity(intent);
                break;
            case R.id.btnManager:
                intent = new Intent(MainActivity.this, StudentManagerActivity.class);
                startActivity(intent);
                break;
            case R.id.btnNews:
                intent = new Intent(MainActivity.this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnMaps:
                intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSocial:
                intent = new Intent(MainActivity.this, SocialActivity.class);
                startActivity(intent);
                break;

        }
    }
}
