package com.nhbaka.assignment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nhbaka.assignment.Model.Student;
import com.nhbaka.assignment.Model.User;
import com.nhbaka.assignment.R;
import com.nhbaka.assignment.SQLite.StudentDAO;

import java.util.ArrayList;

public class StudentAdapter extends BaseAdapter {

    Context context;

    ArrayList<Student> data;



    public StudentAdapter(Context context, ArrayList<Student> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.student_list, null);

        }

        //Ánh xạ
        TextView name = view.findViewById(R.id.tvName);
        TextView birthday = view.findViewById(R.id.tvBirthday);
        TextView stdCode = view.findViewById(R.id.tvNumber);
        TextView tvCount = view.findViewById(R.id.tvCount);


        //Gắn dữ liệu
        Student std = data.get(i);
        stdCode.setText(std.getStdCode());
        name.setText(std.getStdName());
        birthday.setText(std.getBirthday());
        tvCount.setText(std.getIsUsed()+"");
        return view;
    }
}
