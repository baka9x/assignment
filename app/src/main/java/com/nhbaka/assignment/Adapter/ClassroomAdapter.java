package com.nhbaka.assignment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nhbaka.assignment.Model.Classroom;
import com.nhbaka.assignment.R;

import java.util.ArrayList;

public class ClassroomAdapter extends BaseAdapter {

    Context context;
    ArrayList<Classroom> data;


    public ClassroomAdapter(Context context, ArrayList<Classroom> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.classroom_list, null);
        }

        //ánh xạ
        TextView tvNumber = convertView.findViewById(R.id.tvNumber);
        TextView tvClassCode = convertView.findViewById(R.id.tvClassCode);
        TextView tvClassName = convertView.findViewById(R.id.tvClassName);

        //gắn dữ liệu vào
        Classroom classroom = data.get(position);
        int num = position + 1;
        tvNumber.setText(num +"");
        tvClassCode.setText(classroom.getClassCode());
        tvClassName.setText(classroom.getClassName());

        return convertView;
    }
}
