package com.nhbaka.assignment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nhbaka.assignment.Model.News;
import com.nhbaka.assignment.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends BaseAdapter {

    private Context mContext;
    private List<News> mList;

    public NewsAdapter(Context mContext, List<News> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_news, null);

        }

        //Ánh xạ
        ImageView thumbnail = view.findViewById(R.id.thumbnail);

        TextView title = view.findViewById(R.id.news_title);
        TextView date = view.findViewById(R.id.news_date_time);

        //Put data

        News news = mList.get(i);

        title.setText(news.getTitle());
        date.setText(news.getDate());

        Picasso.get().load(news.getThumbnail()).into(thumbnail);


        return view;
    }
}
