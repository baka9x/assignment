package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nhbaka.assignment.Model.Classroom;
import com.nhbaka.assignment.SQLite.ClassroomDAO;


import java.util.ArrayList;

public class AddClassActivity extends AppCompatActivity {

    EditText edtClassNumber, edtClassName;
    Button btnClear, btnSave;
    ClassroomDAO classDao;

    ArrayList<Classroom> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);

        edtClassNumber = findViewById(R.id.edtClassNumber);
        edtClassName = findViewById(R.id.edtClassName);
        btnClear = findViewById(R.id.btnClear);
        btnSave = findViewById(R.id.btnSave);



        classDao = new ClassroomDAO(this);
        list = new ArrayList<>();
        list = classDao.addClassRoom();

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtClassNumber.setText("");
                edtClassName.setText("");
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uname = getIntent().getStringExtra("Username");
                Classroom cl = new Classroom();
                String txtRoomCode = edtClassNumber.getText().toString();
                String txtRoomName = edtClassName.getText().toString();

                //Check validate

                if (TextUtils.isEmpty(txtRoomCode) || TextUtils.isEmpty(txtRoomName)) {
                    Toast.makeText(AddClassActivity.this, "Chưa nhập thông tin lớp", Toast.LENGTH_SHORT).show();
                }

                else {
                    //THÊM LỚP
                    cl.setClassCode(txtRoomCode);
                    cl.setClassName(txtRoomName);

                    if (classDao.insertRoom(cl) == -1){
                        Toast.makeText(AddClassActivity.this, "Thêm không thành công", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(AddClassActivity.this, "Thêm lớp thành công", Toast.LENGTH_SHORT).show();
                    }
                    //Thêm lớp thành công
                    list.add(cl);
                    Intent intent = new Intent(AddClassActivity.this, MainActivity.class);
                    intent.putExtra("Username", uname);
                    startActivity(intent);

                }

            }
        });


    }
}
