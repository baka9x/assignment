package com.nhbaka.assignment.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nhbaka.assignment.Model.Student;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class StudentDAO {
    SQLiteDatabase db, db1;


    public StudentDAO(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
        db1 = dbHelper.getReadableDatabase();

    }

    //CẬP NHẬT TÌNH TRẠNG ĐÃ HỌC DC BAO NHIÊU LỚP
    public int updateIsUsed(Student student, int used) {
        ContentValues values = new ContentValues();
        values.put("STDCODE", student.getStdCode());
        values.put("ISUSED", used);
        return db.update("SINHVIEN", values, "STDCODE=?", new String[]{student.getStdCode()});


    }

    public ArrayList<Student> addStudent() {

        ArrayList<Student> list = new ArrayList<>();
        Cursor c = db.query("SINHVIEN", null, null, null, null, null, null);

        while (c.moveToNext()) {
            Student student = new Student();
            student.setStdCode(c.getString(c.getColumnIndex("STDCODE")));
            student.setStdName(c.getString(c.getColumnIndex("STDNAME")));
            student.setBirthday(c.getString(c.getColumnIndex("BIRTHDAY")));
            student.setClassCode(c.getString(c.getColumnIndex("CLASSCODE")));
            student.setIsUsed(Integer.parseInt(c.getString(c.getColumnIndex("ISUSED"))));
            list.add(student);

        }

        return list;
    }


    public ArrayList<Student> checkStudent(String codeClass) {
        ArrayList<Student> list = new ArrayList<>();

        String sql = "SELECT * FROM SINHVIEN WHERE CLASSCODE LIKE '" + codeClass + "'";
        Cursor c = db1.rawQuery(sql, null);
        while (c.moveToNext()) {
            Student student = new Student();
            student.setStdCode(c.getString(c.getColumnIndex("STDCODE")));
            student.setStdName(c.getString(c.getColumnIndex("STDNAME")));
            student.setBirthday(c.getString(c.getColumnIndex("BIRTHDAY")));
            student.setClassCode(c.getString(c.getColumnIndex("CLASSCODE")));
            student.setIsUsed(Integer.parseInt(c.getString(c.getColumnIndex("ISUSED"))));
            list.add(student);


        }
        return list;
    }


    public void writeCsv(String path, ArrayList<Student> list) {
        //By Đăng
        OutputStreamWriter writer = null;
        try {
            writer = new OutputStreamWriter(new FileOutputStream(path));
            writer.write("CLASS CODE, STUDENT CODE, STUDENT NAME, BIRTHDAY\n");
            for (Student student : list) {
                writer.write(student.getClassCode() + "," +
                        student.getStdCode() + "," +
                        student.getStdName() + "," +
                        student.getBirthday() + "\n");

            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally {
            try {
                assert writer != null;
                writer.flush();
                writer.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }




    /*
    //By source
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(path);
            fileWriter.append("Mã lớp, Mã sinh viên, Tên sinh viên, Ngày sinh\n");
            for (Student student1 : list) {
                fileWriter.append(student1.getClassCode());
                fileWriter.append(",");
                fileWriter.append(student1.getStdCode());
                fileWriter.append(",");
                fileWriter.append(student1.getStdName());
                fileWriter.append(",");
                fileWriter.append(student1.getBirthday());
                fileWriter.append("\n");
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                assert fileWriter != null;
                fileWriter.flush();
                fileWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }


    public long insertStudent(Student student) {
        ContentValues values = new ContentValues();
        values.put("STDCODE", student.getStdCode());
        values.put("STDNAME", student.getStdName());
        values.put("BIRTHDAY", student.getBirthday());
        values.put("CLASSCODE", student.getClassCode());
        values.put("ISUSED", student.getIsUsed());
        return db.insert("SINHVIEN", null, values);
    }

    public int updateStudent(Student student) {
        ContentValues values = new ContentValues();
        values.put("STDCODE", student.getStdCode());
        values.put("STDNAME", student.getStdName());
        values.put("BIRTHDAY", student.getBirthday());
        values.put("CLASSCODE", student.getClassCode());
        return db.update("SINHVIEN", values, "STDCODE=? AND CLASSCODE=?", new String[]{student.getStdCode(), student.getClassCode()});
    }

    public int removeStudent(Student student) {
        return db.delete("SINHVIEN", "STDCODE=? AND CLASSCODE=?", new String[]{student.getStdCode(), student.getClassCode()});
    }


    //CHECK TRÙNG SINH VIÊN TRONG LỚP
    public boolean isDuplicateStd(String code, String code1, String name, String birthday) {
        boolean check = false;
        String query = "SELECT * FROM SINHVIEN";
        Cursor c = db1.rawQuery(query, null);
        String ucode, classCode, uname, ubirthday;

        if (c.moveToFirst()) {
            //check = true;
            do {
                ucode = c.getString(c.getColumnIndex("STDCODE"));
                classCode = c.getString(c.getColumnIndex("CLASSCODE"));
                uname = c.getString(c.getColumnIndex("STDNAME"));
                ubirthday = c.getString(c.getColumnIndex("BIRTHDAY"));

                if (ucode.equals(code) && classCode.equals(code1)) {
                    if (!uname.equals(name)) {
                        check = true;
                        break;
                    } else if (!ubirthday.equals(birthday)) {
                        check = true;
                        break;
                    }
                    check = true;
                }


            } while (c.moveToNext());
        }


        return check;
    }



    //Check Sinh viên trong lớp
    public  boolean isUsed(String classcode) {
        boolean check = false;
        String query = "SELECT STDCODE, CLASSCODE FROM SINHVIEN WHERE CLASSCODE LIKE '" + classcode + "'";
        Cursor c = db1.rawQuery(query, null);
        if (!c.moveToFirst()) {
            check = true;
        }
        return check;
    }


}
