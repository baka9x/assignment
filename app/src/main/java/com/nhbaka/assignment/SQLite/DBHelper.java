package com.nhbaka.assignment.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public final static String DBNAME = "CLASSROOMMANAGER";
    public final static int VERSION = 24 ;

    private final static String CREATE_LOPHOC = "CREATE TABLE IF NOT EXISTS LOPHOC(" +
            "CLASSCODE VARCHAR NOT NULL PRIMARY KEY UNIQUE," +
            "CLASSNAME VARCHAR NOT NULL," +
            "DESCRIPTION VARCHAR NOT NULL)";
    private final static String CREATE_SINHVIEN = "CREATE TABLE IF NOT EXISTS SINHVIEN(" +
            "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
            "STDCODE VARCHAR NOT NULL," +
            "STDNAME VARCHAR NOT NULL," +
            "BIRTHDAY DATETIME NOT NULL," +
            "CLASSCODE VARCHAR," +
            "ISUSED INTEGER NOT NULL DEFAULT(0)," +
            "FOREIGN KEY (CLASSCODE) REFERENCES LOPHOC(CLASSCODE)," +
            "FOREIGN KEY (ISUSED) REFERENCES USERS(ISUSED))";

    private final static String CREATE_LICHTHI = "CREATE TABLE IF NOT EXISTS LICHTHI(" +
            "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
            "TESTNAME VARCHAR NOT NULL," +
            "TESTCOURSE VARCHAR NOT NULL," +
            "TESTDATE VARCHAR NOT NULL," +
            "DURATION VARCHAR NOT NULL," +
            "FOREIGN KEY (TESTCOURSE) REFERENCES LOPHOC(CLASSNAME))";

    private final static String CREATE_USERS = "CREATE TABLE IF NOT EXISTS USERS(" +
            "STDCODE VARCHAR NOT NULL PRIMARY KEY," +
            "PASSWORD VARCHAR NOT NULL," +
            "EMAIL VARCHAR NOT NULL," +
            "TYPE INTEGER NOT NULL DEFAULT(1)," +
            "ISONLINE INTEGER NOT NULL DEFAULT(0)," +
            "ISUSED INTEGER NOT NULL DEFAULT(0)," +
            "FOREIGN KEY (STDCODE) REFERENCES SINHVIEN(STDCODE))";


    public DBHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_SINHVIEN);
        sqLiteDatabase.execSQL(CREATE_LOPHOC);
        sqLiteDatabase.execSQL(CREATE_USERS);
        sqLiteDatabase.execSQL(CREATE_LICHTHI);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS LOPHOC");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS SINHVIEN");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS USERS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS LICHTHI");
        onCreate(sqLiteDatabase);

    }


}
