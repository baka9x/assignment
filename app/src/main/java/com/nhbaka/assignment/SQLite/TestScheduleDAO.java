package com.nhbaka.assignment.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nhbaka.assignment.Model.Schedule;

import java.util.ArrayList;

public class TestScheduleDAO {

    SQLiteDatabase db, db1;

    public TestScheduleDAO(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
        db1 = dbHelper.getReadableDatabase();

    }

    public ArrayList<Schedule> getAllSchedule() {

        ArrayList<Schedule> list = new ArrayList<>();
        Cursor c = db.query("LICHTHI", null, null, null, null, null, null);

        while (c.moveToNext()) {
            Schedule schedule = new Schedule();
            schedule.setId(Integer.parseInt(c.getString(c.getColumnIndex("ID"))));
            schedule.setName(c.getString(c.getColumnIndex("TESTNAME")));
            schedule.setCourseName(c.getString(c.getColumnIndex("TESTCOURSE")));
            schedule.setDate(c.getString(c.getColumnIndex("TESTDATE")));
            schedule.setDuration(c.getString(c.getColumnIndex("DURATION")));
            list.add(schedule);

        }

        return list;
    }


    public long insertSchedule(Schedule schedule) {

        ContentValues values = new ContentValues();
        values.put("TESTNAME", schedule.getName());
        values.put("TESTCOURSE", schedule.getCourseName());
        values.put("TESTDATE", schedule.getDate());
        values.put("DURATION", schedule.getDuration());
        return db.insert("LICHTHI", null, values);

    }


    public int updateSchedule(Schedule schedule) {
        ContentValues values = new ContentValues();
        values.put("ID", schedule.getId());
        values.put("TESTNAME", schedule.getName());
        values.put("TESTCOURSE", schedule.getCourseName());
        values.put("TESTDATE", schedule.getDate());
        values.put("DURATION", schedule.getDuration());

        return db.update("LICHTHI", values, "ID=?", new String[]{String.valueOf(schedule.getId())});

    }

    public int removeSchedule(Schedule schedule) {
        return db.delete("LICHTHI", "ID=?", new String[]{String.valueOf(schedule.getId())});
    }
}
