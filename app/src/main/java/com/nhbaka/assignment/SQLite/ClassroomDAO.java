package com.nhbaka.assignment.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nhbaka.assignment.Model.Classroom;


import java.util.ArrayList;

public class ClassroomDAO {

    SQLiteDatabase db, db1;


    public ClassroomDAO(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
        db1 = dbHelper.getReadableDatabase();

    }

    public ArrayList<Classroom> addClassRoom() {

        ArrayList<Classroom> list = new ArrayList<>();
        Cursor c = db.query("LOPHOC", null, null, null, null, null, null);

        while (c.moveToNext()) {
            Classroom classroom = new Classroom();
            //classroom.setClassNumber(Integer.parseInt(c.getString(c.getColumnIndex("ID"))));
            classroom.setClassCode(c.getString(c.getColumnIndex("CLASSCODE")));
            classroom.setClassName(c.getString(c.getColumnIndex("CLASSNAME")));
            list.add(classroom);

        }

        return list;
    }


    public long insertRoom(Classroom classroom) {

        ContentValues values = new ContentValues();
        values.put("CLASSCODE", classroom.getClassCode());
        values.put("CLASSNAME", classroom.getClassName());
        return db.insert("LOPHOC", null, values);

    }

    public int updateRoom(Classroom classroom) {
        ContentValues values = new ContentValues();
        values.put("CLASSCODE", classroom.getClassCode());
        values.put("CLASSNAME", classroom.getClassName());

        return db.update("LOPHOC", values, "CLASSCODE=?", new String[]{classroom.getClassCode()});

    }

    public int removeRoom(Classroom classroom) {
        return db.delete("LOPHOC", "CLASSCODE=?", new String[]{classroom.getClassCode()});
    }

    public Cursor searchRooms(String text){
        String query = "SELECT * FROM LOPHOC WHERE CLASSNAME='&"+ text + "'&";
        Cursor cursor = db1.rawQuery(query, null);
        return cursor;

    }


}
