package com.nhbaka.assignment.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.nhbaka.assignment.Model.User;

import java.util.ArrayList;

public class UserDAO {
    SQLiteDatabase db, db1;


    public UserDAO(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
        db1 = dbHelper.getReadableDatabase();

    }


    //IMPORTANT*
    //NẾU TÀI KHOẢN ADMIN KHÔNG CÓ THÌ CHECK NULL
    public boolean isNull() {
        boolean check = false;
        String query = "SELECT STDCODE FROM USERS WHERE STDCODE LIKE '%baka%'";

        Cursor c = db1.rawQuery(query, null);
        while (c.moveToNext()) {
            check = true;
            break;
        }

        return check;

    }


    //TÀI KHOẢN ADMIN ĐẸP TRAI NẰM Ở ĐÂY
    public long insertAdmin() {
        ContentValues values = new ContentValues();
        values.put("STDCODE", "baka");
        values.put("PASSWORD", "asdasd");
        values.put("TYPE", 9);
        values.put("ISUSED", 999);
        values.put("EMAIL", "admin@nhbaka.com");

        return db.insert("USERS", null, values);
    }

    //KIỂM TRA PASS KHI LOGIN, ĐÚNG THÌ LOGIN
    public String checkPass(String username) {
        String query = "SELECT STDCODE, PASSWORD FROM USERS";
        Cursor c = db1.rawQuery(query, null);
        String uname, pass;
        pass = "";
        if (c.moveToFirst()) {
            do {
                uname = c.getString(c.getColumnIndex("STDCODE"));
                if (uname.equals(username)) {
                    pass = c.getString(c.getColumnIndex("PASSWORD"));
                    break;
                }
            } while (c.moveToNext());
        }
        return pass;

    }


    //KIỂM TRA email
    public String checkEmail(String username) {
        String query = "SELECT STDCODE, EMAIL FROM USERS";
        Cursor c = db1.rawQuery(query, null);

        String uname, email;

        email = "";
        if (c.moveToFirst()) {
            do {
                uname = c.getString(c.getColumnIndex("STDCODE"));
                if (uname.equals(username)) {
                    email = c.getString(c.getColumnIndex("EMAIL"));
                    break;
                }
            } while (c.moveToNext());
        }
        return email;

    }

    //CẬP NHẬT TÌNH TRẠNG ONLINE 0 = OFFLINE, 1 = ONLINE
    public int updateOnline(User user, int online) {
        ContentValues values = new ContentValues();
        values.put("STDCODE", user.getStdCode());
        values.put("ISONLINE", online);
        return db.update("USERS", values, "STDCODE=?", new String[]{user.getStdCode()});


    }

    //CẬP NHẬT TÌNH TRẠNG ĐÃ HỌC DC BAO NHIÊU LỚP
    public int updateIsUsed(User user, int used) {
        ContentValues values = new ContentValues();
        values.put("STDCODE", user.getStdCode());
        values.put("ISUSED", used);
        return db.update("USERS", values, "STDCODE=?", new String[]{user.getStdCode()});


    }


    //THAY ĐỔI PASSWORD
    public int updatePassword(User user) {
        ContentValues values = new ContentValues();
        values.put("STDCODE", user.getStdCode());
        values.put("PASSWORD", user.getStdPassword());
        return db.update("USERS", values, "STDCODE=?", new String[]{user.getStdCode()});

    }

    //isAdmin(Baka);
    //CHECK TÀI KHOẢN LÀ ADMIN HAY KHÔNG
    public boolean isAdmin(String username) {
        boolean check = false;

        String query = "SELECT STDCODE, TYPE FROM USERS WHERE TYPE=9";
        Cursor c = db1.rawQuery(query, null);
        String uname;

        if (c.moveToFirst()) {
            do {
                uname = c.getString(c.getColumnIndex("STDCODE"));
                if (uname.equals(username)) {
                    check = true;
                    break;
                }

            } while (c.moveToNext());
        }


        return check;
    }

    //TÀI KHOẢN ĐÃ SỬ DỤNG ĐĂNG KÝ LỚP HỌC KHÁC SẼ KHÔNG XÓA
    public int isUsed(String username) {
        int used = 0;
        String uname;
        String query = "SELECT STDCODE, ISUSED FROM USERS WHERE ISUSED > 0";
        Cursor c = db1.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {

                uname = c.getString(c.getColumnIndex("STDCODE"));
                if (uname.equals(username)) {
                    used = Integer.parseInt(c.getString(c.getColumnIndex("ISUSED")));
                    break;
                }

            } while (c.moveToNext());
        }


        return used;
    }


    //Người đang online
    public String isOnline() {
        String query = "SELECT STDCODE, ISONLINE FROM USERS WHERE ISONLINE=1";
        Cursor c = db1.rawQuery(query, null);
        String uname = "";
        if (!c.moveToFirst()) {
            Log.d("Thông báo", "Không có ai online");
        } else {
            uname = c.getString(c.getColumnIndex("STDCODE"));
        }
        return uname;
    }


    public ArrayList<User> showUser() {

        ArrayList<User> list = new ArrayList<>();
        Cursor c = db.query("USERS", null, null, null, null, null, null);

        while (c.moveToNext()) {
            User user = new User();
            user.setStdCode(c.getString(c.getColumnIndex("STDCODE")));
            user.setStdPassword(c.getString(c.getColumnIndex("PASSWORD")));
            user.setType(Integer.parseInt(c.getString(c.getColumnIndex("TYPE"))));
            user.setEmail(c.getString(c.getColumnIndex("EMAIL")));
            //user.setIsOnline();
            user.setIsUsed(Integer.parseInt(c.getString(c.getColumnIndex("ISUSED"))));
            list.add(user);
        }

        return list;
    }


    public long insertUser(User user) {
        ContentValues values = new ContentValues();
        values.put("STDCODE", user.getStdCode());
        values.put("PASSWORD", 123456);
        values.put("EMAIL", user.getEmail());
        values.put("TYPE", 1);
        values.put("ISONLINE", 0);
        values.put("ISUSED", user.getIsUsed());
        return db.insert("USERS", null, values);
    }


    //tài khoản đăng ký lớp học
    public int isUsedd(String name) {

        String query = "SELECT STDCODE, ISUSED FROM USERS WHERE STDCODE LIKE '" + name +"'";
        Cursor c = db1.rawQuery(query, null);
        int uuse = 0;
        if (!c.moveToFirst()) {
            Log.d("Thông báo", "Không có tài khoản");
        } else {
            uuse = Integer.parseInt(c.getString(c.getColumnIndex("ISUSED")));

        }
        return uuse;
    }

    public boolean removeUser(User user) {


        return db.delete("USERS", "STDCODE=?", new String[]{user.getStdCode()}) < 1;
    }




}
