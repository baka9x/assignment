package com.nhbaka.assignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.nhbaka.assignment.SQLite.UserDAO;

public class ForgetPasswordActivity extends AppCompatActivity {

    TextInputEditText email;
    Button btnChangePassword;
    UserDAO userDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Khôi phục mật khẩu");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        email = findViewById(R.id.email);
        btnChangePassword = findViewById(R.id.btnChangePassword);

        userDAO = new UserDAO(this);

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                String userName = intent.getStringExtra("USERNAME");
                String Email = email.getText().toString().trim();
                String txtEmail = userDAO.checkEmail(userName);
                if (Email.equals(txtEmail)) {
                    Intent intent1 = new Intent(ForgetPasswordActivity.this, ChangePasswordActivity.class);
                    startActivity(intent1);
                }else{
                    Toast.makeText(ForgetPasswordActivity.this, "Email của bạn không đúng", Toast.LENGTH_SHORT).show();
                }
            }
        });




    }


}
